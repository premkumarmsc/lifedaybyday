//
//  EmployeeCell
//  365Assignment
//
//  Created by Udhaya on 10/30/13.
//  Copyright (c) 2013 UdhayaChandrika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityCell : UITableViewCell
@property(nonatomic,retain )IBOutlet UIImageView *img;
@property(nonatomic,retain )IBOutlet UILabel *titleName;
@property(nonatomic,retain )IBOutlet UILabel *titleNumber;
@end
