//
//  ViewController.h
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIMonthYearPicker;

@interface ViewController : UIViewController<UIMonthYearPickerDelegate>{
    NSDateFormatter *dateFormatter;
}

@property (weak, nonatomic) IBOutlet UIMonthYearPicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property(retain,nonatomic)IBOutlet UITableView *tblView;
@property(nonatomic,retain)IBOutlet UIView *monthPickerView;
@property(nonatomic,retain)IBOutlet UILabel *monthAndYearLabel;
@property(nonatomic,retain) IBOutlet UIImageView *noEvent;
-(IBAction)monthPickerDone:(id)sender;
-(IBAction)monthPickerCancel:(id)sender;
-(IBAction)changeMonth:(id)sender;
-(IBAction)settingButton:(id)sender;
-(void)newFuntion;
@end
