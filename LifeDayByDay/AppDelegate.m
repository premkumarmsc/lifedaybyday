//
//  AppDelegate.m
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    [add setObject:@"10:00 AM" forKey:@"ALWAYS_TIME"];
    
    
    NSDateFormatter *currentYearFormat = [[NSDateFormatter alloc] init];
    [currentYearFormat setDateFormat:@"ddmmyyyyhhmm"];
    NSString *date =[currentYearFormat stringFromDate:[NSDate date]];
    
    NSLog(@"Date:%@",date);
    
    
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc] initWithIdentifier:@"VALUE_ID" accessGroup:nil];
    [wrapper setObject:date forKey:(__bridge id)kSecAttrService];
    
       
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    if (IS_IPHONE_5) {
        
          self.viewController = [[ViewController alloc] initWithNibName:@"ViewController5" bundle:nil];
        
    }
    else
    {
         self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil]; 
    }
    
  
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification NS_AVAILABLE_IOS(4_0)
{
    NSLog(@"## Received Push Badge: ");
    
    NSLog(@"The push Data:");
    
    //[[MPMusicPlayerController applicationMusicPlayer] setVolume:1.0];
    
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateActive)
    {
        NSLog(@"ACTIVE");
        
        [self showNotificationAlert:notification];
    }
    else
    {
        NSLog(@"NOT ACTIVE");
        
        NSLog(@"Not:%@",notification.alertBody);
        
        
        if([notification.alertBody length]!=0)
        {
        
       // [[UIApplication sharedApplication] cancelAllLocalNotifications];
       // [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        
        
        NSLog(@"Badget:%ld",(long)notification.applicationIconBadgeNumber);
        
        
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
        //notification.applicationIconBadgeNumber = 0;
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        // [check setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"SELECTED_INDEX"];
        
        NSString *selIndex=[NSString stringWithFormat:@"%ld",(long)notification.applicationIconBadgeNumber];
        
        [check setObject:selIndex forKey:@"NOTIFIED_INDEX"];
        
      
        
        
            NSString *notificationName = @"Test";
            
            NSString *key = @"Value";
            
            NSDictionary *dictionary = [NSDictionary dictionaryWithObject:notification.alertBody forKey:key];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:dictionary];
            
        }
        
    }
    
    
}



- (void)showNotificationAlert:(UILocalNotification *)alertMsg
{
    
    
    
    
    if (!alertMsg)
        return;
    
    
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", nil)
                                                    message:alertMsg.alertBody
                                                   delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
    
     [[UIApplication sharedApplication] cancelLocalNotification:alertMsg];
      //alertMsg.applicationIconBadgeNumber = 0;
    
   // [[UIApplication sharedApplication] cancelAllLocalNotifications];
  //  [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    
    
    
    
    
    
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    UIApplication *app = [UIApplication sharedApplication];
    
    //create new uiBackgroundTask
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    //and create new timer with async call:
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //run function methodRunAfterBackground
      
        NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(methodRunAfterBackground) userInfo:nil repeats:YES];
        
       
        
        
        [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
        [[NSRunLoop currentRunLoop] run];
    });
    
    
   
    
    
}


-(void)methodRunAfterBackground
{
    
   // NSLog(@"Enter");
    
    
    ViewController *view=[[ViewController alloc]init];
    [view newFuntion];
    
   
}



- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
