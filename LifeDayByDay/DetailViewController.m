//
//  ViewController.m
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import "DetailViewController.h"
#import "ActivityCell.h"
#import <QuartzCore/QuartzCore.h>

@interface DetailViewController ()

@end

@implementation DetailViewController
@synthesize datePicker;
@synthesize dateLabel;
NSMutableArray *monthYearArr;
NSMutableArray *idArr;
NSMutableArray *dateArr;
NSMutableArray *timeArr;
NSMutableArray *descriptionArr;
NSMutableArray *idnewsArr;
NSMutableArray *imageArr;
NSMutableArray *titleArr;
int indexSelection;

ActivityCell *cell;


- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic

{//start function
    
    NSString *key = @"Value";
    
    NSDictionary *dictionary = [notification userInfo];
    
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    NSLog(@"Device orientation Settings –> %@",stringValueToUse);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    [check setObject:stringValueToUse forKey:@"LAST_NOTIFIED_MESSAGE"];
    
    if (IS_IPHONE_5) {
        PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
    }
    else
    {
    PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
    
    
}//end function
-(IBAction)img1
{
    NSLog(@"IMG1");
}

-(IBAction)clickImage:(id)sender
{
    
    NSLog(@"Click");
    
    _ImageBack.hidden=NO;
}
-(IBAction)dismissImage:(id)sender
{
    _ImageBack.hidden=YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:_monthPickerView];
    [self.view addSubview:_ImageBack];
    _ImageBack.hidden=YES;
       
    datePicker._delegate = self;
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(useNotificationWithString:)
                                                 name:@"Test"
                                               object:nil];
    

   
  
       
    
       
    
    [self getDB];
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)getDB
{
    NSLog(@"RECEIVED");
    
    
    NSUserDefaults *valueGet=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getlastMonth=[valueGet objectForKey:@"LAST_MONTH_YEAR"];
    NSString *getlastDate=[valueGet objectForKey:@"LAST_DATE"];
      NSString *getselectedDate=[valueGet objectForKey:@"SELECTED_DATE"];
    
    
    NSLog(@"GET LAST MONTH:%@",getlastMonth);
    NSLog(@"GET LAST DATE:%@",getlastDate);
    

    
    
    monthYearArr=[[NSMutableArray alloc]init];
    idArr=[[NSMutableArray alloc]init];
    idnewsArr=[[NSMutableArray alloc]init];
    descriptionArr=[[NSMutableArray alloc]init];
    titleArr=[[NSMutableArray alloc]init];
    dateArr=[[NSMutableArray alloc]init];
    timeArr=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
    
    
    
    NSString *querystring=[NSString stringWithFormat:@"select month_year,id,Date,Time,Description,ID_News,image,Title from tb_datas where Day='%@'",getselectedDate];
    const char *sqlvalue = [querystring UTF8String];
    Dataware *dbsql=[[Dataware alloc]initDataware];
    sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    if(sqlStmt != nil)
        while(sqlite3_step(sqlStmt) == SQLITE_ROW)
        {
            
            //NSLog(@"UU");
            
            NSString *monthYearStr;
            NSString *idStr;
            NSString *dateStr;
            NSString *Timestr;
            NSString *desStr;
            NSString *idNewsStr;
            NSString *imageStr;
            NSString *title;
            
            
            @try {
                monthYearStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception)
            {
                monthYearStr=@"";
            }
            
            [monthYearArr addObject:monthYearStr];
            
            
            @try {
                idStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 1)];
                
            }
            @catch (NSException *exception)
            {
                idStr=@"";
            }
            
            [idArr addObject:idStr];
            
            @try {
                dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 2)];
                
            }
            @catch (NSException *exception)
            {
                dateStr=@"";
            }
            
            [dateArr addObject:dateStr];
            
            @try {
                Timestr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 3)];
                
            }
            @catch (NSException *exception)
            {
                Timestr=@"";
            }
            
            [timeArr addObject:Timestr];
            
            @try {
                desStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 4)];
                
            }
            @catch (NSException *exception)
            {
                desStr=@"";
            }
            
            [descriptionArr addObject:desStr];
            
            @try {
                idNewsStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 5)];
                
            }
            @catch (NSException *exception)
            {
                idNewsStr=@"";
            }
            
            [idnewsArr addObject:idNewsStr];
            
            @try {
                imageStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 6)];
                
            }
            @catch (NSException *exception)
            {
                imageStr=@"";
            }
            
            [imageArr addObject:imageStr];
            
            @try {
                title=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 7)];
                
            }
            @catch (NSException *exception)
            {
                title=@"";
            }
            
            [titleArr addObject:title];
            
            
            
            
            
        }
    
    NSLog(@"BOOK ID:%@",titleArr);
    
   
    
    
    CALayer *imageLayer1 = _imageVie.layer;
    [imageLayer1 setCornerRadius:30];
    [imageLayer1 setBorderWidth:0];
    [imageLayer1 setMasksToBounds:YES];
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    // [check setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"SELECTED_INDEX"];
    
    
      NSString *selIndex=[check objectForKey:@"SELECTED_INDEX"];
    
    
    for (int i=0; i<[titleArr count]; i++)
    {
        
        if ([selIndex isEqualToString:titleArr[i]])
        {
            indexSelection=i;
        }
        
        
    }
    
    
    
    
    
    
  
    
  //  indexSelection=[selIndex intValue];
    
    
    _txtView.font=[UIFont fontWithName:@"OpenSans-Light" size:15.00f];
     _labeltxt.font=[UIFont fontWithName:@"BirchStd" size:20.00f];
    // dateLabel.font=[UIFont fontWithName:@"OpenSans-Semibold" size:18.00f];
    
    
    _labeltxt.text=[NSString stringWithFormat:@"%@",titleArr[indexSelection]];
    
    
   // NSString *summary = [[[descriptionArr[indexSelection] stringByStrippingTags] stringByRemovingNewLinesAndWhitespace] stringByDecodingHTMLEntities];
    
    
    //NSString *summary = [[[descriptionArr[indexSelection] stringByStrippingTags] stringByRemovingNewLinesAndWhitespace] stringByDecodingHTMLEntities];
    
    
    _txtView.text=[NSString stringWithFormat:@"%@",  descriptionArr[indexSelection]];
  
  
    
    _imageVie.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
     _imageVie1.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
    
    
    
    
    NSString *dateString=dateArr[indexSelection];
    
   dateString= [dateString stringByReplacingOccurrencesOfString:@"/" withString:@":"];
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd:MM:yyyy"];
    
    NSDate *newDate=[dateFormatter1 dateFromString:dateString];
    
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"dd MMM yyyy"];
    
     dateLabel.text=[dateFormatter2 stringFromDate:newDate];
    
    
    
}




-(IBAction)previousButton:(id)sender
{
    if (indexSelection==0) {
        
        indexSelection=0;
        CALayer *imageLayer1 = _imageVie.layer;
        [imageLayer1 setCornerRadius:30];
        [imageLayer1 setBorderWidth:0];
        [imageLayer1 setMasksToBounds:YES];
                    
        
        _labeltxt.text=[NSString stringWithFormat:@"%@",titleArr[indexSelection]];
        _txtView.text=[NSString stringWithFormat:@"%@",descriptionArr[indexSelection]];
        
        
        _imageVie.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
        _imageVie1.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];

        
        NSString *dateString=dateArr[indexSelection];
        
        dateString= [dateString stringByReplacingOccurrencesOfString:@"/" withString:@":"];
        
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd:MM:yyyy"];
        
        NSDate *newDate=[dateFormatter1 dateFromString:dateString];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd MMM yyyy"];
        
        dateLabel.text=[dateFormatter2 stringFromDate:newDate];
    }
    else
    {
        indexSelection= indexSelection-1;
        CALayer *imageLayer1 = _imageVie.layer;
        [imageLayer1 setCornerRadius:30];
        [imageLayer1 setBorderWidth:0];
        [imageLayer1 setMasksToBounds:YES];
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        // [check setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"SELECTED_INDEX"];
        
     
        _imageVie.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
        _imageVie1.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];

        
        _labeltxt.text=[NSString stringWithFormat:@"%@",titleArr[indexSelection]];
        _txtView.text=[NSString stringWithFormat:@"%@",descriptionArr[indexSelection]];
        
      
        
        NSString *dateString=dateArr[indexSelection];
        
        dateString= [dateString stringByReplacingOccurrencesOfString:@"/" withString:@":"];
        
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd:MM:yyyy"];
        
        NSDate *newDate=[dateFormatter1 dateFromString:dateString];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd MMM yyyy"];
        
        dateLabel.text=[dateFormatter2 stringFromDate:newDate];
    }
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
   
    _monthPickerView.hidden=NO;

}
-(IBAction)nextButton:(id)sender
{
    if (indexSelection==[idnewsArr count]-1) {
        
        indexSelection=[idnewsArr count]-1;
        CALayer *imageLayer1 = _imageVie.layer;
        [imageLayer1 setCornerRadius:30];
        [imageLayer1 setBorderWidth:0];
        [imageLayer1 setMasksToBounds:YES];
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        // [check setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"SELECTED_INDEX"];
      
        _imageVie.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
        _imageVie1.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];

        
        _labeltxt.text=[NSString stringWithFormat:@"%@",titleArr[indexSelection]];
        _txtView.text=[NSString stringWithFormat:@"%@",descriptionArr[indexSelection]];
             
        NSString *dateString=dateArr[indexSelection];
        
        dateString= [dateString stringByReplacingOccurrencesOfString:@"/" withString:@":"];
        
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd:MM:yyyy"];
        
        NSDate *newDate=[dateFormatter1 dateFromString:dateString];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd MMM yyyy"];
        
        dateLabel.text=[dateFormatter2 stringFromDate:newDate];
    }
    else
    {
        indexSelection=indexSelection+1;
        CALayer *imageLayer1 = _imageVie.layer;
        [imageLayer1 setCornerRadius:30];
        [imageLayer1 setBorderWidth:0];
        [imageLayer1 setMasksToBounds:YES];
        
        NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
        // [check setObject:[NSString stringWithFormat:@"%d",indexPath.row] forKey:@"SELECTED_INDEX"];
        
      
        _imageVie.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];
        _imageVie1.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imageArr[indexSelection]]];

        
        _labeltxt.text=[NSString stringWithFormat:@"%@",titleArr[indexSelection]];
        _txtView.text=[NSString stringWithFormat:@"%@",descriptionArr[indexSelection]];
        
        NSString *dateString=dateArr[indexSelection];
        
        dateString= [dateString stringByReplacingOccurrencesOfString:@"/" withString:@":"];
        
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd:MM:yyyy"];
        
        NSDate *newDate=[dateFormatter1 dateFromString:dateString];
        
        
        NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
        [dateFormatter2 setDateFormat:@"dd MMM yyyy"];
        
        dateLabel.text=[dateFormatter2 stringFromDate:newDate];
    }
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
    
    _monthPickerView.hidden=NO;

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)backButton:(id)sender
{
    if (IS_IPHONE_5) {
        ViewController *setting=[[ViewController alloc]initWithNibName:@"ViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];

    }
    else
    {
    ViewController *setting=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
}

@end
