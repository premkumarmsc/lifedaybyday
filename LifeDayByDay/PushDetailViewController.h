//
//  ViewController.h
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UIMonthYearPicker;

@interface PushDetailViewController : UIViewController<UIMonthYearPickerDelegate>{
    NSDateFormatter *dateFormatter;
}

@property (weak, nonatomic) IBOutlet UIMonthYearPicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property(retain,nonatomic)IBOutlet UITableView *tblView;
@property(nonatomic,retain)IBOutlet UIView *monthPickerView;
@property(nonatomic,retain)IBOutlet UILabel *monthAndYearLabel;

@property(nonatomic,retain)IBOutlet UIImageView *imageVie;
@property(nonatomic,retain)IBOutlet UITextView *txtView;
@property(nonatomic,retain)IBOutlet UILabel *labeltxt;

@property(nonatomic,retain)IBOutlet UIView *ImageBack;
@property(nonatomic,retain)IBOutlet UIImageView *imageVie1;
-(IBAction)clickImage:(id)sender;
-(IBAction)dismissImage:(id)sender;

-(IBAction)monthPickerDone:(id)sender;
-(IBAction)monthPickerCancel:(id)sender;
-(IBAction)changeMonth:(id)sender;
-(IBAction)backButton:(id)sender;


-(IBAction)previousButton:(id)sender;
-(IBAction)nextButton:(id)sender;


@end
