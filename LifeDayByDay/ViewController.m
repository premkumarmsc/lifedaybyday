//
//  ViewController.m
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import "ViewController.h"
#import "ActivityCell.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
@property (strong) NSArray *array;
@end

@implementation ViewController
@synthesize datePicker;
@synthesize dateLabel;
@synthesize array = _array;
NSDateFormatter *formatter;
ActivityCell *cell;



NSMutableArray *monthYearArr;
NSMutableArray *idArr;
NSMutableArray *dateArr;
NSMutableArray *timeArr;
NSMutableArray *descriptionArr;
NSMutableArray *idnewsArr;
NSMutableArray *imageArr;
NSMutableArray *titleArr;
NSMutableArray *dayArr;

NSMutableArray *notifications;
UILocalNotification *prototypeNotification;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    CALayer* mask = [[CALayer alloc] init];
    [mask setBackgroundColor: [UIColor blackColor].CGColor];
    [mask setFrame:  CGRectMake(10.0f, 10.0f, 300.0f, 196.0f)];
    [mask setCornerRadius: 5.0f];
    [datePicker.layer setMask: mask];
   // [mask release];
    
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(useNotificationWithString:)
                                                 name:@"Test"
                                               object:nil];

   
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
       
    
    NSString *getDBUpdation=[check objectForKey:@"DB_UPDATE"];
    
    
    NSLog(@"HELLo:%@",getDBUpdation);
    
    
    if ([getDBUpdation isEqualToString:@"NO"])
    {
        [self getDB];
        
       // [self updateDB];
    }
    else
    {
        [self updateDB];
    }

   

    
}
- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic

{//start function
    
    NSString *key = @"Value";
    
    NSDictionary *dictionary = [notification userInfo];
    
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    NSLog(@"Device orientation –> %@",stringValueToUse);
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    [check setObject:stringValueToUse forKey:@"LAST_NOTIFIED_MESSAGE"];
    
    
    if (IS_IPHONE_5) {
        PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
    }
    else
    {
    PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
    
}//end function



-(void)newFuntion
{
    //NSLog(@"NEW");
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd/MM/yyyy"];
    
    NSString *str=[dateFormatter1 stringFromDate:[NSDate date]];
    
    
 
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:[NSDate date]];
    NSInteger currentHour = [components hour];
    
    //NSLog(@"Current Hour:%d",currentHour);
    
    NSInteger currentMinute = [components minute];
    NSInteger currentSecond = [components second];
    
    if (currentHour < 2 || (currentHour > 1 || (currentHour == 2 && (currentMinute > 0 || currentSecond > 0))))
    {
        
      //  NSLog(@"YES YES");
        
         NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
        
        NSString *getFromStored=[isFirst objectForKey:str];
        
        
        if (![getFromStored isEqualToString:@"YES"])
        {
            
            
            
            [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
            
            NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
            NSString *getDBUpdation=[check objectForKey:@"DB_UPDATE"];
            
            if ([getDBUpdation isEqualToString:@"NO"])
            {
                [self getDB];
            }
            else
            {
                [self updateDB];
            }
            
            
            [check setObject:@"YES" forKey:str];

        }
        
       
        
    }
    
    
      

    
}



- (void) receiveTestNotification:(NSNotification *) notification
{
    
    NSLog(@"Enter Here");
    
    if ([[notification name] isEqualToString:@"TestNotification"])
    {
        NSLog (@"Successfully received the test notification!");
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Hello" message:notification.object delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)getDB
{
    NSLog(@"RECEIVED");
    
    
    NSUserDefaults *valueGet=[NSUserDefaults standardUserDefaults];
    
    
    NSString *getlastMonth=[valueGet objectForKey:@"LAST_MONTH_YEAR"];
    NSString *getlastDate=[valueGet objectForKey:@"LAST_DATE"];
    
    
    
   
    
    
    NSLog(@"GET LAST MONTH ASAS:%@",getlastMonth);
    NSLog(@"GET LAST DATE ASAS:%@",getlastDate);
    
    
    [self.view addSubview:_monthPickerView];
    
    if (IS_IPHONE_5) {
         _monthPickerView.frame=CGRectMake(0, 290, 320, 330);
    }
    else
    {
    
    _monthPickerView.frame=CGRectMake(0, 202, 320, 330);
    }
    _monthPickerView.hidden=YES;
    
    datePicker._delegate = self;
    dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"MMMM   yyyy"];
    
    [dateFormatter setDateFormat:@"MM:yyyy"];
    
   
    
    NSDate* newDate = [dateFormatter dateFromString:getlastMonth];
    
  
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMM yyyy hh:mm a"];
    dateLabel.text = [dateFormatter1 stringFromDate:[NSDate date]];
    
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MMMM yyyy"];
    
    [datePicker setDate:newDate];
    
    
    
    _monthAndYearLabel.text = [formatter stringFromDate:newDate];
   

    monthYearArr=[[NSMutableArray alloc]init];
    idArr=[[NSMutableArray alloc]init];
    idnewsArr=[[NSMutableArray alloc]init];
    descriptionArr=[[NSMutableArray alloc]init];
    titleArr=[[NSMutableArray alloc]init];
    dateArr=[[NSMutableArray alloc]init];
    timeArr=[[NSMutableArray alloc]init];
    imageArr=[[NSMutableArray alloc]init];
     dayArr=[[NSMutableArray alloc]init];
   
    
    
    NSString *querystring=[NSString stringWithFormat:@"select month_year,id,Date,Time,Description,ID_News,image,Title,Day from tb_datas where month_year='%@'",getlastMonth];
    const char *sqlvalue = [querystring UTF8String];
    Dataware *dbsql=[[Dataware alloc]initDataware];
    sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    if(sqlStmt != nil)
        while(sqlite3_step(sqlStmt) == SQLITE_ROW)
        {
            
            //NSLog(@"UU");
            
            NSString *monthYearStr;
            NSString *idStr;
            NSString *dateStr;
            NSString *Timestr;
            NSString *desStr;
            NSString *idNewsStr;
            NSString *imageStr;
            NSString *title;
             NSString *day;
           
            
            @try {
                monthYearStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception)
            {
                monthYearStr=@"";
            }
            
            [monthYearArr addObject:monthYearStr];
            
            
            @try {
                idStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 1)];
                
            }
            @catch (NSException *exception)
            {
                idStr=@"";
            }
            
            [idArr addObject:idStr];
            
            @try {
                dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 2)];
                
            }
            @catch (NSException *exception)
            {
                dateStr=@"";
            }
            
            [dateArr addObject:dateStr];
            
            @try {
                Timestr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 3)];
                
            }
            @catch (NSException *exception)
            {
                Timestr=@"";
            }
            
            [timeArr addObject:Timestr];
            
            @try {
                desStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 4)];
                
            }
            @catch (NSException *exception)
            {
                desStr=@"";
            }
            
            [descriptionArr addObject:desStr];
            
            @try {
                idNewsStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 5)];
                
            }
            @catch (NSException *exception)
            {
                idNewsStr=@"";
            }
            
            [idnewsArr addObject:idNewsStr];
            
            @try {
                imageStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 6)];
                
            }
            @catch (NSException *exception)
            {
                imageStr=@"";
            }
            
            [imageArr addObject:imageStr];
            
            @try {
                title=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 7)];
                
            }
            @catch (NSException *exception)
            {
                title=@"";
            }
            
            [titleArr addObject:title];
            
          
            
            @try {
                day=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 8)];
                
            }
            @catch (NSException *exception)
            {
                day=@"";
            }
            
            [dayArr addObject:day];
            
            
            
        }
    
    NSLog(@"BOOK ID:%@",dayArr);
         [self tomorrowEvents];
    
    
    if ([dayArr count]==0) {
        
        _noEvent.hidden=NO;
    }
    else
    {
        _noEvent.hidden=YES;
    }
    
    
    [_tblView reloadData];

   
    NSString *getlastMonth1=[valueGet objectForKey:@"LAST_MONTH_YEAR"];
    NSString *getlastDate1=[valueGet objectForKey:@"LAST_DATE"];
    
    
    
    
    
    
    NSLog(@"GET LAST MONTH:%@",getlastMonth1);
    NSLog(@"GET LAST DATE:%@",getlastDate1);
    
    
    [self.view addSubview:_monthPickerView];
    
    if (IS_IPHONE_5) {
        _monthPickerView.frame=CGRectMake(0, 290, 320, 330);
    }
    else
    {
        
        _monthPickerView.frame=CGRectMake(0, 202, 320, 330);
    }
    _monthPickerView.hidden=YES;
    
    datePicker._delegate = self;
    dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"MMMM   yyyy"];
    
    [dateFormatter setDateFormat:@"MM:yyyy"];
    
    
    
    NSDate* newDate1 = [dateFormatter dateFromString:getlastMonth1];
    
    
    [datePicker awakeFromNib];
    
    
    NSDateFormatter *dateFormatter11 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMM yyyy hh:mm a"];
    dateLabel.text = [dateFormatter11 stringFromDate:[NSDate date]];
    
    formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"MMMM yyyy"];
    
    [datePicker setDate:newDate1];
    
    
    
    [datePicker reloadAllComponents];
    
    
    _monthAndYearLabel.text = [formatter stringFromDate:newDate1];
    
    

}

-(void)tomorrowEvents
{
    
    
    NSDate *now = [NSDate date];
    int daysToAdd = 1;
    NSDate *newDate1 = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
    NSDateFormatter *currentDate = [[NSDateFormatter alloc] init];
    [currentDate setDateFormat:@"dd:MM"];
    NSString *getCurrentDate=[currentDate stringFromDate:now];
    NSLog(@"Current Date:%@",now);
    
    // TESTING PURPOOSE
    
    //getCurrentDate=@"02:03";
    
    
    
    
    NSDateFormatter *currentYearFormat = [[NSDateFormatter alloc] init];
    [currentYearFormat setDateFormat:@"YYYY"];
    NSString *currentYear=[currentYearFormat stringFromDate:[NSDate date]];
    
    NSLog(@"Current year fggf:%@",currentYear);
    
    NSMutableArray *titleCheck=[[NSMutableArray alloc]init];
    
    
    
    
    
    NSMutableArray * monthYearArrAlert=[[NSMutableArray alloc]init];
     NSMutableArray *idArrAlert=[[NSMutableArray alloc]init];
    NSMutableArray * idnewsArrAlert=[[NSMutableArray alloc]init];
    NSMutableArray * descriptionArrAlert=[[NSMutableArray alloc]init];
     NSMutableArray *titleArrAlert=[[NSMutableArray alloc]init];
    NSMutableArray * dateArrAlert=[[NSMutableArray alloc]init];
    NSMutableArray * timeArrAlert=[[NSMutableArray alloc]init];
     NSMutableArray *imageArrAlert=[[NSMutableArray alloc]init];
      NSMutableArray *yearArrAlert=[[NSMutableArray alloc]init];
    
    
    NSString *querystring=[NSString stringWithFormat:@"select month_year,id,Date,Time,Description,ID_News,image,Title from tb_datas where date_month='%@'",getCurrentDate];
    const char *sqlvalue = [querystring UTF8String];
    Dataware *dbsql=[[Dataware alloc]initDataware];
    sqlite3_stmt *sqlStmt=[dbsql OpenSQL:sqlvalue];
    
    if(sqlStmt != nil)
        while(sqlite3_step(sqlStmt) == SQLITE_ROW)
        {
            
            //NSLog(@"UU");
            
            NSString *monthYearStr;
            NSString *idStr;
            NSString *dateStr;
            NSString *Timestr;
            NSString *desStr;
            NSString *idNewsStr;
            NSString *imageStr;
            NSString *title;
            
            
            @try {
                monthYearStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 0)];
                
            }
            @catch (NSException *exception)
            {
                monthYearStr=@"";
            }
            
            [monthYearArrAlert addObject:monthYearStr];
            
            
            @try {
                idStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 1)];
                
            }
            @catch (NSException *exception)
            {
                idStr=@"";
            }
            
            [idArrAlert addObject:idStr];
            
            @try {
                dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 2)];
                
            }
            @catch (NSException *exception)
            {
                dateStr=@"";
            }
            
            
            NSArray* foo = [dateStr componentsSeparatedByString: @"/"];
             NSString* year=[foo objectAtIndex: 2];
            [yearArrAlert addObject:year];
            [dateArrAlert addObject:dateStr];
            
            @try {
                Timestr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 3)];
                
            }
            @catch (NSException *exception)
            {
                Timestr=@"";
            }
            
            [timeArrAlert addObject:Timestr];
            
            @try {
                desStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 4)];
                
            }
            @catch (NSException *exception)
            {
                desStr=@"";
            }
            
            [descriptionArrAlert addObject:desStr];
            
            @try {
                idNewsStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 5)];
                
            }
            @catch (NSException *exception)
            {
                idNewsStr=@"";
            }
            
            [idnewsArrAlert addObject:idNewsStr];
            
            @try {
                imageStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 6)];
                
            }
            @catch (NSException *exception)
            {
                imageStr=@"";
            }
            
            [imageArrAlert addObject:imageStr];
            
            @try {
                title=[NSString stringWithUTF8String:(char *)sqlite3_column_text(sqlStmt, 7)];
                
            }
            @catch (NSException *exception)
            {
                title=@"";
            }
            
            [titleArrAlert addObject:title];
            
            
            
            
            
        }
    
    NSLog(@"BOOK ID 1:%@",timeArrAlert);
    
    
    
    
    if ([timeArrAlert count]!=0)
    {
        
        
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        NSString *getType=[add objectForKey:@"ALERT_TYPE"];
        
        NSLog(@"DISPLAY_PIN:%@",getType);
        
        
        NSString *alarmTime=@"";
        
        
        if ([getType length]==0)
        {
            NSLog(@"EXACT");
            alarmTime=timeArrAlert[0];
        }
        else if([getType isEqualToString:@"NO"])
        {
            NSLog(@"STOP");
            alarmTime=@"0";
        }
        
        else if([getType isEqualToString:@"EXACT"])
        {
            NSLog(@"EXACT");
            alarmTime=timeArrAlert[0];
        }
        
        else if([getType isEqualToString:@"PREFERRED"])
        {
            NSLog(@"PREFERRED");
            NSString *getPreferredTime=[add objectForKey:@"PREFFERED_TIME"];
            
            
            NSDateFormatter *currentYearFormat = [[NSDateFormatter alloc] init];
            [currentYearFormat setDateFormat:@"hh:mm a"];
            NSDate *date =[currentYearFormat dateFromString:getPreferredTime];
            
            NSDateFormatter *currentYearFormat1 = [[NSDateFormatter alloc] init];
            [currentYearFormat1 setDateFormat:@"HH:mm"];
            
            alarmTime=[currentYearFormat1 stringFromDate:date];
            
            
        }
        else if([getType isEqualToString:@"ALWAYS"])
        {
            NSLog(@"ALWAYS");
            alarmTime=@"10:00";
        }
        
        
        NSDateFormatter *currentDateToday = [[NSDateFormatter alloc] init];
        [currentDateToday setDateFormat:@"dd:MM:yyyy"];
        
        NSLog(@"ALERM TIME:%@",alarmTime);
        NSLog(@"ALERM Date:%@",[currentDateToday stringFromDate:[NSDate date]]);
        NSLog(@"Current Year:%@",currentYear);
        
        
        NSMutableArray *alarmTimearray=[[NSMutableArray alloc]init];
        NSMutableArray *alarmDatearray=[[NSMutableArray alloc]init];
        NSMutableArray *alarmyearArray=[[NSMutableArray alloc]init];
        NSMutableArray *currentTimeArray=[[NSMutableArray alloc]init];
        
        
        NSDateFormatter *currentTimeToday = [[NSDateFormatter alloc] init];
        [currentTimeToday setDateFormat:@"HH:mm"];
        NSString *currentTime=[currentTimeToday stringFromDate:[NSDate date]];
        
        if ([alarmTime intValue]!=0) {
            
            
            
            for (int i=0; i<[monthYearArrAlert count]; i++)
            {
                [alarmTimearray addObject:alarmTime];
                [alarmDatearray addObject:[currentDateToday stringFromDate:[NSDate date]]];
                
                int year_distance=[currentYear intValue]-[yearArrAlert[i]intValue];
                
                
                
                NSString *makeMessage=[NSString stringWithFormat:@"%@ %d years ago %@",dateArrAlert[i],year_distance,titleArrAlert[i]];
                [currentTimeArray addObject:currentTime];
                
                [alarmyearArray addObject:[NSString stringWithFormat:@"%@",makeMessage]];
            }
            
            
            NSLog(@"Title:%@",alarmyearArray);
            NSLog(@"Alarm_Time:%@",alarmTimearray);
            NSLog(@"Alarm date:%@",alarmDatearray);
            
            
            
            
            NSMutableArray *finalArray=[[NSMutableArray alloc]init];
            
            
            for (int i=0; i<[alarmyearArray count]; i++)
            {
                
                
                
                
                
                NSString *appendDate=[NSString stringWithFormat:@"%@ %@:01",alarmDatearray[i],alarmTimearray[i]];
                
                
                NSDateFormatter *currentDateFormatter = [[NSDateFormatter alloc] init];
                [currentDateFormatter setDateFormat:@"dd:MM:yyyy HH:mm:ss"];
                
                
                NSDate * date=[currentDateFormatter dateFromString:appendDate];
                
                
                //NSLog(@"Date:%@",date);
                
                
                
                NSDateFormatter *currentDateFormatter1 = [[NSDateFormatter alloc] init];
                [currentDateFormatter1 setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
                
                
                NSString *string=[currentDateFormatter1 stringFromDate:date];
                [finalArray addObject:string];
                
                //NSLog(@"String:%@",string);
                
                          
                
                
            }
            
            NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
            
            
            NSString *checkRepeat=[isFirst objectForKey:@"CHECK_IS_FIRST"];
            
            if (![checkRepeat isEqualToString:@"NO"])
            {
                [self notification:alarmyearArray datearray:finalArray];
            }
            
            
            
            
        }
        else
        {
            
           // [[UIApplication sharedApplication] cancelAllLocalNotifications];
            
        }

    }
    
       
    
}
-(void)notification:(NSMutableArray *)nameArray datearray:(NSMutableArray*)datearray;
{
    
    
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"NO" forKey:@"CHECK_IS_FIRST"];
    
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    
    NSLog(@"Final Array:%@",datearray);
    
    
    NSDateFormatter *Form = [[NSDateFormatter alloc]init];
    [Form setTimeZone:[NSTimeZone localTimeZone]];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    
    [Form setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    
    //NSString *checkdate=@"20/11/2013 17:42:15";
    
    
    for (int i=0; i<[datearray count]; i++)
    {
        
        NSString *checkdate=datearray[i];
        
        NSDate *date=[Form dateFromString:checkdate];
        
        
        
       
        NSDate * today = [NSDate date];
        NSComparisonResult result = [today compare:date];
        switch (result)
        {
            case NSOrderedAscending:
            {
                NSLog(@"Future Date");
                
                [notification setFireDate:date];
                NSLog(@" Notificcation %@",notification.fireDate);
                
                
                
                NSString *soundFile=[[NSString alloc] initWithFormat:@"Lullaby.wav"];
                notification.applicationIconBadgeNumber = i+1;
                notification.timeZone = [NSTimeZone localTimeZone];
                notification.alertBody = [NSString stringWithFormat:@"%@",[nameArray objectAtIndex:i]];
                notification.alertAction = @"View";
                notification.soundName = soundFile;
                [[UIApplication sharedApplication] scheduleLocalNotification:notification];

                
            }
            case NSOrderedDescending:
            {
                //NSLog(@"Earlier Date");
            }
            default:
            {
              //  NSLog(@"Error Comparing Dates");
            }
        }
        
        
        
      }
    
   
    
    /*
    for (int i=0;i<datearray.count;i++)
    {
        //[Form setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        //NSDate *date =[Form dateFromString:[datearray objectAtIndex:i]];
        [Form setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        //NSDate *date =[Form dateFromString:[[datearray objectAtIndex:i] stringByReplacingOccurrencesOfString:@"/" withString:@"-"]];
       // NSLog(@"Final Str:%@",date);
        
        
               
        
        
        
        NSDate *date=datearray[i];
        
         NSLog(@"Final Str:%@",date);
        
        if(notification)
        {
           
            
            //comment this for current date time
            [notification setFireDate:date];
            NSLog(@" Notificcation %@",notification.fireDate);
            
          
            NSString *soundFile=[[NSString alloc] initWithFormat:@"Lullaby.wav"];
            notification.applicationIconBadgeNumber = i;
            notification.timeZone = [NSTimeZone localTimeZone];
            notification.alertBody = [NSString stringWithFormat:@"%@",[nameArray objectAtIndex:i]];
            notification.alertAction = @"View";
            notification.soundName = soundFile;
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
        
         
       // NSLog(@"fire date%@",  notification.fireDate);
    }
     */


}

-(void)updateDB
{
    NSString *file = [[NSBundle mainBundle] pathForResource:@"data" ofType:@"txt"];
    
    NSString *str =[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil];
    
//    NSString *jsonString = @"{\"ID\":{\"Content\":268,\"type\":\"text\"},\"ContractTemplateID\":{\"Content\":65,\"type\":\"text\"}}";
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
     NSLog(@"json:%@",json);
    
    
    NSMutableArray *titleArray=[[NSMutableArray alloc]init];
    NSMutableArray *lastMonthArray=[[NSMutableArray alloc]init];
    NSMutableArray *lastDateArray=[[NSMutableArray alloc]init];
    
    
    
   // NSString *file1=[file stringByReplacingOccurrencesOfString:@";" withString:@","];
    
    int finalValue,s=0;
    
    for (NSDictionary *get in json )
    {
       
        NSString *IDNEWS=[NSString stringWithFormat:@"%@",[get objectForKey:@"IDNEWS"]];
        NSString *DATE=[NSString stringWithFormat:@"%@",[get objectForKey:@"DATE"]];
        NSString *DESCRIPTION=[NSString stringWithFormat:@"%@",[get objectForKey:@"DESCRIPTION"]];
        NSString *TIME=[NSString stringWithFormat:@"%@",[get objectForKey:@"TIME"]];
        NSString *TITLE=[NSString stringWithFormat:@"%@",[get objectForKey:@"TITLE"]];
        NSString *IMAGENAME=[NSString stringWithFormat:@"%@",[get objectForKey:@"IMAGENAME"]];
        
        
        
        DESCRIPTION=[DESCRIPTION stringByReplacingOccurrencesOfString:@"�" withString:@""];
        
        
        Dataware *dd=[[Dataware alloc]initDataware];
        
        NSString *sstr=@"";
        
        sstr=@"INSERT INTO tb_datas(Date,Time,Description,ID_News,image,Title,month_year,date_month,Day) VALUES(?,?,?,?,?,?,?,?,?)";
        
        
        NSArray* foo = [DATE componentsSeparatedByString: @"/"];
        NSString* month = [foo objectAtIndex: 1];
        NSString* year=[foo objectAtIndex: 2];
        NSString* dateStr = [foo objectAtIndex: 0];
        
        
        NSString *appendstr=[NSString stringWithFormat:@"%@:%@",month,year];
        NSString *appendstrDate=[NSString stringWithFormat:@"%@:%@",dateStr,month];
        
        
        sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
        if (stmt!=nil)
        {
            sqlite3_bind_text(stmt,1,[DATE UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,2, [TIME UTF8String],-1 ,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,3, [DESCRIPTION UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,4, [IDNEWS UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,5, [IMAGENAME UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,6, [TITLE UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,7, [appendstr UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,8, [appendstrDate UTF8String],-1,SQLITE_TRANSIENT);
            sqlite3_bind_text(stmt,9, [dateStr UTF8String],-1,SQLITE_TRANSIENT);
            
            sqlite3_step(stmt);
        }
        sqlite3_finalize(stmt);
        
        [titleArray addObject:year];
        [lastMonthArray addObject:appendstr];
        [lastDateArray addObject:DATE];
        
        
        
        s++;
        finalValue=s-1;
        
            }
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:@"NO" forKey:@"DB_UPDATE"];
    
    
    NSLog(@"LAST MONTH:%@",titleArray[finalValue]);
    
    [check setObject:titleArray[0] forKey:@"MIN_YEAR"];
    [check setObject:titleArray[finalValue] forKey:@"MAX_YEAR"];
    [check setObject:lastMonthArray[finalValue] forKey:@"LAST_MONTH_YEAR"];
    [check setObject:lastDateArray[finalValue] forKey:@"LAST_DATE"];
    
    NSLog(@"Title_Array:%@",lastMonthArray[finalValue] );
    
    [self getDB];
    
    //[self viewDidLoad];
    
//    NSDictionary *dic=[NSDictionary dictionaryWithContentsOfFile:file];
//    NSLog(@"dic:%@",dic);
    
    
    
    
    
    
    
    
//    NSArray *arrayOfArrays = [CSVParser parseCSVIntoArrayOfArraysFromFile:file
//                                             withSeparatedCharacterString:@"|"
//                                                     quoteCharacterString:@"\""];
    
    
    /*
    NSArray *arrayOfArrays = [CSVParser parseCSVIntoArrayOfArraysFromFile:file
                                             withSeparatedCharacterString:@"|"
                                                     quoteCharacterString:nil];
    
    
    NSLog(@"%@",arrayOfArrays);
   */
   
    /*
    [CSVParser parseCSVIntoArrayOfDictionariesFromFile:file
                          withSeparatedCharacterString:@"|"
                                  quoteCharacterString:nil
                                             withBlock:^(NSArray *array, NSError *error)
    {
                                                 self.array = array;
                                                 NSLog(@"%@", self.array);
                                                 
                                                 
                                                 NSLog(@"Array:%@",array);
                                                 
                                                 int finalValue;
                                                 
                                                 for (int i=0; i<[array count]; i++) {
                                                     
                                                    
                                                     
                                                     
                                                     
                                                     Dataware *dd=[[Dataware alloc]initDataware];
                                                     
                                                     NSString *sstr=@"";
                                                     
                                                     sstr=@"INSERT INTO tb_datas(Date,Time,Description,ID_News,image,Title,month_year,date_month,Day) VALUES(?,?,?,?,?,?,?,?,?)";
                                                     
                                                     
                                                     NSArray* foo = [[array[i] objectForKey:@"Date"] componentsSeparatedByString: @"/"];
                                                     NSString* month = [foo objectAtIndex: 1];
                                                     NSString* year=[foo objectAtIndex: 2];
                                                      NSString* dateStr = [foo objectAtIndex: 0];
                                                     
                                                     
                                                     NSString *appendstr=[NSString stringWithFormat:@"%@:%@",month,year];
                                                      NSString *appendstrDate=[NSString stringWithFormat:@"%@:%@",dateStr,month];
                                                     
                                                     
                                                     sqlite3_stmt *stmt=[dd OpenSQL:[sstr UTF8String]];
                                                     if (stmt!=nil)
                                                     {
                                                         sqlite3_bind_text(stmt,1,[[array[i] objectForKey:@"Date"] UTF8String],-1,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,2, [[array[i] objectForKey:@"Time"] UTF8String],-1 ,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,3, [[array[i] objectForKey:@"Description"] UTF8String],-1,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,4, [[array[i] objectForKey:@"IDNEws"] UTF8String],-1,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,5, [[array[i] objectForKey:@"ImageName"] UTF8String],-1,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,6, [[array[i] objectForKey:@"Title"] UTF8String],-1,SQLITE_TRANSIENT);
                                                         sqlite3_bind_text(stmt,7, [appendstr UTF8String],-1,SQLITE_TRANSIENT);
                                                          sqlite3_bind_text(stmt,8, [appendstrDate UTF8String],-1,SQLITE_TRANSIENT);
                                                          sqlite3_bind_text(stmt,9, [dateStr UTF8String],-1,SQLITE_TRANSIENT);
                                                         
                                                         sqlite3_step(stmt);
                                                     }
                                                     sqlite3_finalize(stmt);
                                                     
                                                      [titleArray addObject:year];
                                                     [lastMonthArray addObject:appendstr];
                                                       [lastDateArray addObject:[array[i] objectForKey:@"Date"]];
                                                     
                                                     finalValue=i;
                                                 }
                                                 
                                                 
                                                 NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
                                                 [check setObject:@"NO" forKey:@"DB_UPDATE"];
                                                 
                                                 [check setObject:titleArray[0] forKey:@"MIN_YEAR"];
                                                 [check setObject:titleArray[finalValue] forKey:@"MAX_YEAR"];
                                                 [check setObject:lastMonthArray[finalValue] forKey:@"LAST_MONTH_YEAR"];
                                                  [check setObject:lastDateArray[finalValue] forKey:@"LAST_DATE"];
                                                 
                                               NSLog(@"Title_Array:%@",lastMonthArray[finalValue] );
                                                 
                                                 [self getDB];
                                                 
//                                             }
//    ];
     
     */
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)monthPickerDone:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
    
    
    [self getDB];
}
-(IBAction)monthPickerCancel:(id)sender
{
  
    
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
    
    
}
-(IBAction)changeMonth:(id)sender
{
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
    
    _monthPickerView.hidden=NO;
    
}

#pragma mark - UIMonthYearPickerDelegate
- (void) pickerView:(UIPickerView *)pickerView didChangeDate:(NSDate *)newDate{
    
    
    _monthAndYearLabel.text = [formatter stringFromDate:newDate];
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MM:yyyy"];
    
    
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    [add setObject:[dateFormatter1 stringFromDate:newDate] forKey:@"LAST_MONTH_YEAR"];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

-(IBAction)settingButton:(id)sender
{
    
    
    if (IS_IPHONE_5) {
        SettingsViewController *setting=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
    }
    else
    {
    SettingsViewController *setting=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
     setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [titleArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    ActivityCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ActivityCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (ActivityCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    
       
    cell.titleName.text=[NSString stringWithFormat:@"%@",titleArr[indexPath.row]];
    cell.titleNumber.text=[NSString stringWithFormat:@"%@",dayArr[indexPath.row]];
        
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
    
    
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = @"sample title";
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 53;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSLog(@"Select %d",indexPath.row);
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    [check setObject:[NSString stringWithFormat:@"%@",titleArr[indexPath.row]] forKey:@"SELECTED_INDEX"];
    [check setObject:[NSString stringWithFormat:@"%@",dayArr[indexPath.row]] forKey:@"SELECTED_DATE"];
    
    if (IS_IPHONE_5) {
        DetailViewController *setting=[[DetailViewController alloc]initWithNibName:@"DetailViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
    }
    else
    {
    DetailViewController *setting=[[DetailViewController alloc]initWithNibName:@"DetailViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
    
    /*
    PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
     */
}


@end
