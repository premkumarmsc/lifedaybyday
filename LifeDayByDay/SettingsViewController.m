//
//  ViewController.m
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import "SettingsViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface SettingsViewController ()

@end

@implementation SettingsViewController


- (void)useNotificationWithString:(NSNotification *)notification //use notification method and logic

{//start function
    
    NSString *key = @"Value";
    
    NSDictionary *dictionary = [notification userInfo];
    
    NSString *stringValueToUse = [dictionary valueForKey:key];
    
    NSLog(@"Device orientation Settings –> %@",stringValueToUse);
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    
    [check setObject:stringValueToUse forKey:@"LAST_NOTIFIED_MESSAGE"];
    
    if (IS_IPHONE_5) {
        PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
    }
    else
    {
    PushDetailViewController *setting=[[PushDetailViewController alloc]initWithNibName:@"PushDetailViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
    
    
}//end function


- (void)viewDidLoad
{
    
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    _exactSwitch.on=NO;
    _alwaysSwitch.on=NO;
    _preferredSwitch.on=NO;
    
    
    
    CALayer* mask = [[CALayer alloc] init];
    [mask setBackgroundColor: [UIColor whiteColor].CGColor];
    [mask setFrame:  CGRectMake(80.0f, 10.0f, 155.0f, 196.0f)];
    [mask setCornerRadius: 5.0f];
    [_preferredPicker.layer setMask: mask];
    
    
    
    
    
    
    
    
    
    
    


     _settings.font=[UIFont fontWithName:@"BirchStd" size:25.00f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(useNotificationWithString:)
                                                 name:@"Test"
                                               object:nil];

    
    
    [super viewDidLoad];
    [self.view addSubview:_monthPickerView];
    
    if (IS_IPHONE_5) {
        _monthPickerView.frame=CGRectMake(0, 290, 320, 330);
    }
    else
    {
        
        _monthPickerView.frame=CGRectMake(0, 202, 320, 330);
    }
    

    //_monthPickerView.frame=CGRectMake(0, 202, 320, 330);
    _monthPickerView.hidden=YES;
    
    
    [self.view addSubview:_monthPickerView1];
    
    
    
    if (IS_IPHONE_5) {
        _monthPickerView1.frame=CGRectMake(0, 290, 320, 330);
    }
    else
    {
        
        _monthPickerView1.frame=CGRectMake(0, 202, 320, 330);
    }
    
   
    _monthPickerView1.hidden=YES;
    
     
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MMM yyyy hh:mm a"];
    _monthAndYearLabel.text = [dateFormatter1 stringFromDate:[NSDate date]];
    
    
    
    
    NSString *getPreferredTime=[add objectForKey:@"PREFFERED_TIME"];
     NSString *getAlwaysTime=[add objectForKey:@"ALWAYS_TIME"];
    
    
    NSDateFormatter *dateFormatterPreferred = [[NSDateFormatter alloc] init];
    [dateFormatterPreferred setDateFormat:@"hh:mm a"];
    
    if ([getPreferredTime length]==0)
    {
       
        _preferredTimeLabel.text =[NSString stringWithFormat:@"Preffered time %@",[dateFormatterPreferred stringFromDate:[NSDate date]]];
    }
    else
    {
      _preferredTimeLabel.text =[NSString stringWithFormat:@"Preffered time %@",getPreferredTime];  
    }
    
    
    
    if ([getAlwaysTime length]==0)
    {
        _alwaysTimeLabel.text =[NSString stringWithFormat:@"Always as %@",[dateFormatterPreferred stringFromDate:[NSDate date]]];
    }
    else
    {
        _alwaysTimeLabel.text =[NSString stringWithFormat:@"Always as %@",getAlwaysTime];
    }
    
    
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    NSString *getType=[add objectForKey:@"ALERT_TYPE"];

    NSLog(@"DISPLAY_PIN:%@",getType);
    
    
    if ([getType length]==0) {
        _exactSwitch.on=YES;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"EXACT" forKey:@"ALERT_TYPE"];
    }
    else if([getType isEqualToString:@"NO"])
    {
        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"NO" forKey:@"ALERT_TYPE"];
    }
    
    else if([getType isEqualToString:@"EXACT"])
    {
        _exactSwitch.on=YES;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"EXACT" forKey:@"ALERT_TYPE"];
    }
    
    else if([getType isEqualToString:@"PREFERRED"])
    {
        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=YES;
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"PREFERRED" forKey:@"ALERT_TYPE"];
    }
    else if([getType isEqualToString:@"ALWAYS"])
    {
        _exactSwitch.on=NO;
        _alwaysSwitch.on=YES;
        _preferredSwitch.on=NO;
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"ALWAYS" forKey:@"ALERT_TYPE"];
    }
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)prefferedClick:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
      _monthPickerView.hidden=YES;
     _monthPickerView.hidden=NO;
}
-(IBAction)alwaysClick:(id)sender
{
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
     _monthPickerView1.hidden=YES;
     _monthPickerView1.hidden=NO;
}

-(IBAction)preffereddatePicker:(id)sender
{
    
    
 

    
    
    
    NSDateFormatter *dateFormatterPreferred = [[NSDateFormatter alloc] init];
    [dateFormatterPreferred setDateFormat:@"hh:mm a"];
    
           
        _preferredTimeLabel.text =[NSString stringWithFormat:@"Preffered time %@",[dateFormatterPreferred stringFromDate:[_preferredPicker date]]];
   
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    [add setObject:[dateFormatterPreferred stringFromDate:[_preferredPicker date]] forKey:@"PREFFERED_TIME"];

    
   
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
    

}
-(IBAction)alwaysdatePicker:(id)sender
{
    NSDateFormatter *dateFormatterPreferred = [[NSDateFormatter alloc] init];
    [dateFormatterPreferred setDateFormat:@"hh:mm a"];

        _alwaysTimeLabel.text =[NSString stringWithFormat:@"Always as %@",[dateFormatterPreferred stringFromDate:[_alwaysPicker date]]];
    NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
    [add setObject:[dateFormatterPreferred stringFromDate:[_alwaysPicker date]] forKey:@"ALWAYS_TIME"];
    
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
}


-(IBAction)monthPickerDone:(id)sender
{
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
    _monthPickerView.hidden=YES;
     _monthPickerView1.hidden=YES;
    
}
-(IBAction)monthPickerCancel:(id)sender
{
    
    CATransition *transition = [CATransition animation];
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    transition.duration = 0.5;
    transition.type = kCAScrollNone;
    [self.view.layer addAnimation:transition forKey:nil];
    
  _monthPickerView.hidden=YES;
     _monthPickerView1.hidden=YES;
}

-(IBAction)backButton:(id)sender
{
    if (IS_IPHONE_5) {
        ViewController *setting=[[ViewController alloc]initWithNibName:@"ViewController5" bundle:nil];
        setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:setting animated:YES completion:nil];
        
    }
    else
    {
    ViewController *setting=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    setting.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
    [self presentViewController:setting animated:YES completion:nil];
    }
}
- (IBAction)exactSwitchFlip:(id)sender
{
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (_exactSwitch.on)
    {
        _exactSwitch.on=YES;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"EXACT" forKey:@"ALERT_TYPE"];
        
        
    }
    else
    {
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"NO" forKey:@"ALERT_TYPE"];
        
        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        
    }
    
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
    
}
- (IBAction)preferredSwitchFlip:(id)sender
{
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (_preferredSwitch.on)
    {
        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=YES;
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"PREFERRED" forKey:@"ALERT_TYPE"];

    }
    else
    {
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"NO" forKey:@"ALERT_TYPE"];

        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        
    }
    
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
}
- (IBAction)alwaysSwitchFlip:(id)sender
{
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (_alwaysSwitch.on)
    {
        _exactSwitch.on=NO;
        _alwaysSwitch.on=YES;
        _preferredSwitch.on=NO;
        
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"ALWAYS" forKey:@"ALERT_TYPE"];

    }
    else
    {
        NSUserDefaults *add=[NSUserDefaults standardUserDefaults];
        [add setObject:@"NO" forKey:@"ALERT_TYPE"];

        
        _exactSwitch.on=NO;
        _alwaysSwitch.on=NO;
        _preferredSwitch.on=NO;
        
    }
    
    NSUserDefaults *isFirst=[NSUserDefaults standardUserDefaults];
    [isFirst setObject:@"YES" forKey:@"CHECK_IS_FIRST"];
}

@end
