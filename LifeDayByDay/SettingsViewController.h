//
//  ViewController.h
//  LifeDayByDay
//
//  Created by ephronsystems on 11/19/13.
//  Copyright (c) 2013 PREMKUMAR. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface SettingsViewController : UIViewController{
    NSDateFormatter *dateFormatter;
}


@property(nonatomic,retain)IBOutlet UIView *monthPickerView;
@property(nonatomic,retain)IBOutlet UIView *monthPickerView1;
@property(nonatomic,retain)IBOutlet UILabel *monthAndYearLabel;


@property(nonatomic,retain)IBOutlet UILabel *preferredTimeLabel;
@property(nonatomic,retain)IBOutlet UILabel *alwaysTimeLabel;

@property(nonatomic,retain)IBOutlet UILabel *settings;

@property (nonatomic, retain) IBOutlet UISwitch *exactSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *preferredSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *alwaysSwitch;


@property(nonatomic,retain)IBOutlet UIDatePicker *preferredPicker;
@property(nonatomic,retain)IBOutlet UIDatePicker *alwaysPicker;

-(IBAction)monthPickerDone:(id)sender;
-(IBAction)monthPickerCancel:(id)sender;
-(IBAction)changeMonth:(id)sender;
-(IBAction)backButton:(id)sender;


- (IBAction)exactSwitchFlip:(id)sender;
- (IBAction)preferredSwitchFlip:(id)sender;
- (IBAction)alwaysSwitchFlip:(id)sender;


-(IBAction)prefferedClick:(id)sender;
-(IBAction)alwaysClick:(id)sender;


-(IBAction)preffereddatePicker:(id)sender;
-(IBAction)alwaysdatePicker:(id)sender;


@end
