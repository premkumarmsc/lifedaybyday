//
//  Dataware
//  365Assignment
//
//  Created by Udhaya on 10/30/13.
//  Copyright (c) 2013 UdhayaChandrika. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface Dataware : NSObject 
{
	NSString *databaseName;
	NSString *databasePath;
	sqlite3 *dbsql;
}

@property (nonatomic, retain) NSString *databaseName;
@property (nonatomic, retain) NSString *databasePath;

-(id)initDataware;
-(sqlite3_stmt *)OpenSQL:(const char *)stmt;
- (void)CloseSQL;

@end
