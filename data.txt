[
  {
    "IDNEWS":1,
    "TITLE":"Nasce il RE!",
    "DATE":"08/01/1935",
    "TIME":"10:00",
    "DESCRIPTION":"sotto il segno del capricorno, in una piccola abitazione a Tupelo, Mississipi, nasce la leggenda del rock: il suo nome � Elvis Aaron Presley.",
    "IMAGENAME":"19350801_elvis"
  },
  {
    "IDNEWS":2,
    "TITLE":"Una bicicletta mancata",
    "DATE":"07/01/1941",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis spasima per una bicicletta che purtroppo (o per fortuna) � molto cara, cos� la madre Gladys decide di regalargli per il suo compleanno dei sei anni che avver� domani una chitarra trovata in un negozio dell'usato del valore di 12 dollari e 95 centesimi",
    "IMAGENAME":"19410107_elvis"
  },
  {
    "IDNEWS":3,
    "TITLE":"Il primo contratto discografico",
    "DATE":"12/07/1954",
    "TIME":"10:00",
    "DESCRIPTION":"Sam Phillips, della Sun Records, ascolta un brano di Elvis in un sottoscala e ne rimane folgorato| sborsa 4 dollari e firma il primo contratto con Presley: un piccolo investimento per una vera gallina dalle uova d'oro. I primi brani lo dimostreranno subito.",
    "IMAGENAME":"19540712_elvis"
  },
  {
    "IDNEWS":4,
    "TITLE":"Prima apparizione  di Elvis a uno show radiofonico di musica country",
    "DATE":"16/10/1954",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis e il suo gruppo appaiono per la prima volta al \"Lousiana Hayride\", uno show radiofonico di musica country, trasmesso dalla KWKH Radio.  \nA novembre dello stesso anno Elvis firma un contratto per un anno, ossia esibizioni per cinquantadue sabati notte. E' una grande occasione, ma mentre la popolarit� di Elvis cresce, il suo impegno con l'Hayride lo porta a viaggiare moltissimo per potersi far conoscere. E' proprio in questo periodo che Elvis incontra TOM PARKER, detto \"Il Colonnello\", gi� manager di artisti vari e in rapporti proprio con il \"Louisiana Hayride\" ",
    "IMAGENAME":"19541016_elvis"
  },
  {
    "IDNEWS":5,
    "TITLE":"un desiderio che si avverer�",
    "DATE":"31/10/1954",
    "TIME":"10:00",
    "DESCRIPTION":"\"se diventassi famoso come Eddie Arnold, mi piacerebbe avere i Jordanaires come coristi\".",
    "IMAGENAME":"19541031_elvis"
  },
  {
    "IDNEWS":6,
    "TITLE":"Concerto in un posto malfamato",
    "DATE":"23/11/1954",
    "TIME":"10:00",
    "DESCRIPTION":"Pantaloni e giubbotto bianco, camicia nera. Il Round Up Club godeva di pessima fama per l'altrettanto brutta reputazione della sua clientela. Elvis aveva bisogno di lavorare per cui accett� l'ingaggio senza troppe remore. La sera del concerto incontr� un suo amico molto povero e lo port� a mangiare al ristorante. Nonostante il suo successo Elvis non ha mai dimenticato le sue origini.",
    "IMAGENAME":"19541123_elvis"
  },
  {
    "IDNEWS":7,
    "TITLE":"Elvis, Scotty and Bill nella sun Studios",
    "DATE":"03/02/1955",
    "TIME":"10:00",
    "DESCRIPTION":" Elvis, Scotty e Bill prendono tempo per lavorare su nuove canzoni in studio durante questa settimana. Durante questo periodo si registrano 'Baby Giochiamo House', che sar� il lato A del loro prossimo singolo \" along with still\" versioni inedita fino al 1999. Una versione di Ray Charles '' I Got a Woman\" e \"Trying to Get to You\". Dopo la sessione Stan Kesler, un chitarrista d'acciaio che lavorava principalmente per la Sun STudios, va a casa e scrive quello che diventer� il lato B, \"I'm Left, You're Right, She's Gone\", basato sulla melodia dello spot di una zuppa. Durante questa settimana il trio appare anche a programmi scolastici a Messick High School e Messick Junior High per aiutare Sonny Neal, il figlio di Bob, nella sua campagna per il Consiglio degli studenti.",
    "IMAGENAME":"19550203_elvis"
  },
  {
    "IDNEWS":8,
    "TITLE":"Elvis va a New York",
    "DATE":"10/04/1955",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis raggiunse New York, allo scopo di effettuare un provino per poter partecipare al programma \"Talent Scouts\", condotto dal critico Arthur Godfrey, ma ricevette da quest'ultimo una decisa risposta negativa",
    "IMAGENAME":"19550410_elvis"
  },
  {
    "IDNEWS":9,
    "TITLE":"Troppo pieno all'edizione 1955 del Country Music Jamboree",
    "DATE":"05/08/1955",
    "TIME":"10:00",
    "DESCRIPTION":"Il giornale di Memphis il 6 agosto ha riferito che le strade erano bloccatate con troppa gente arrivata per lo spettacolo e che diverse centinaia sono stati allontanati. Il dilettante fotografo locale Robert Dye che ha fotografato Elvis alla Shell il 5 agosto, ha riportato che ha prestato la sua chiatarra ad Elvis il quale dopo una pessima performance gli restitu� la chitarra con due corde rotte!",
    "IMAGENAME":"19550805_elvis"
  },
  {
    "IDNEWS":10,
    "TITLE":"Elvis firma un contratto, con il quale d� i diritti di manager alla \"Hank Snow Attraction\"",
    "DATE":"15/08/1955",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis firma un contratto, con il quale d� i diritti di manager alla \"Hank Snow Attraction\", che � posseduta equamente da Snow e dal Colonnello Parker. Bob Neal resta in funzione di consigliere. Molto presto il Colonnello Parker riesce a diventare il manager in esclusiva di Elvis e rester� tale fino alla morte del cantante.",
    "IMAGENAME":"19550815_elvis"
  },
  {
    "IDNEWS":11,
    "TITLE":"Elvis firma il suo primo contratto con la \"RCA Records\" senza precedenti: 40.000 $, con un bonus di 5.000$ per Elvis!!!",
    "DATE":"20/11/1955",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis firma il suo primo contratto con la \"RCA Records\". E' il Colonnello Parker a negoziare la vendita del contratto tra la \"Sun\" e la \"RCA\", che include i primi cinque singoli incisi e tanto altro materiale. Il prezzo della transazione � senza precedenti: 40.000 $, con un bonus di 5.000$ per Elvis!!!",
    "IMAGENAME":"19551120_elvis"
  },
  {
    "IDNEWS":12,
    "TITLE":"Elvis Registra \"Heartbreak Hotel\"",
    "DATE":"05/01/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Heartbreak Hotel � un singolo del cantante rock statunitense Elvis Presley, pubblicato il 27 gennaio 1956 dall'etichetta discografica RCA. La canzone � stata scritta da Mae Axton e Tommy Durden (viene spesso accreditata anche allo stesso Elvis Presley) ed � stata suonata e cantata dal cantante all'Ed Sullivan Show durante le sue celebri apparizioni nel programma. Nel 2004 la canzone � stata inserita alla posizione numero 45 nella lista delle 500 migliori canzoni di sempre redatta dalla rivista Rolling Stone. Il singolo � stato ripubblicato sotto forma di EP nel maggio dello stesso anno e successivamente il 10 agosto 2007.",
    "IMAGENAME":"19560105_elvis"
  },
  {
    "IDNEWS":13,
    "TITLE":"Elvis realizza il suo desiderio: avere i Jordanaires come coristi",
    "DATE":"10/01/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis registra la sua prima sessione per la RCA nello studio di Nashville. Tra le canzoni di questa session c'� la famosissima \"HEARTBREAK HOTEL\". THE JORDANAIRES, quartetto gospel/country, inizia a lavorare con lui in sala di registrazione e lo segue anche nelle esibizioni dal vivo. \nIl gruppo apparir� con Elvis anche in alcuni suoi film e rester� con lui fino alla fine degli anni '60.10 gennaio 1956, due giorni dopo il suo ventunesimo compleanno, Elvis registra la sua prima sessione per la RCA nello studio di Nashville. Tra le canzoni di questa session c'� la famosissima \"HEARTBREAK HOTEL\". THE JORDANAIRES, quartetto gospel/country, inizia a lavorare con lui in sala di registrazione e lo segue anche nelle esibizioni dal vivo. \nIl gruppo apparir� con Elvis anche in alcuni suoi film e rester� con lui fino alla fine degli anni '60.",
    "IMAGENAME":"19560110_elvis"
  },
  {
    "IDNEWS":14,
    "TITLE":"Elvis per Prima volta in TV",
    "DATE":"28/01/1956",
    "TIME":"10:00",
    "DESCRIPTION":"apparve per la prima volta in uno show trasmesso dalla televisione nazionale, lo Stage Show dei fratelli Dorsey, riscuotendo un buon successo",
    "IMAGENAME":"19560128_elvis"
  },
  {
    "IDNEWS":15,
    "TITLE":"Elvis va a fare un provino in cui suona una chitarra senza corde!",
    "DATE":"01/04/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis va ad Hollywood per un provino per la \"Paramount Studios\", ed interpreta la canzone \"Blue Suede Shoes\". La canzone viene fatta cantare in playback e la chitarra che gli viene data per l'esibizione no ha nemmeno le corde.... Sempre in aprile Elvis fa la sua apparizione al \"The Milton Berle Show\" sulla ABC e firma un contratto con Hal Wallis e la \"Paramount Pictures\". \nTra la fine di aprile e l'inizio di maggio 1956 Elvis accetta un ingaggio di due settimane al New Frontier Hotel di Las Vegas, ma purtroppo egli non � ci� che piace agli adulti giocatori d'azzardo del posto. E' in queste due settimane per� che sia il singolo \"Heartbreak Hotel\" che l'album \"Elvis Presley\" sono in vetta alla Billboard's pop chart. A parte questo flop a Las Vegas, la popolarit� di Elvis cresce sempre di pi�, le folle diventano sempre pi� grandi alle sue esibizioni, alcune delle quali terminano prima a causa dei fans che sommergono il palco: Elvis scatena un vero e proprio pandemonio in ogni posto vada!",
    "IMAGENAME":"19560401_elvis"
  },
  {
    "IDNEWS":16,
    "TITLE":"Elvis va in TV con 40 milioni di spettatori",
    "DATE":"03/04/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis prende parte ad uno degli spettacoli TV pi� visti, il Milton Berle Show| 40 milioni di spettatori assistono entusiasti alle sue esibizioni, ma i milioni sono davvero molti per quanto riguarda i suoi guadagni e per le dimensioni di vendita dei suoi dischi.",
    "IMAGENAME":"19560403_elvis"
  },
  {
    "IDNEWS":17,
    "TITLE":"Una versione troppo sensuale di \"Hound dog\"!",
    "DATE":"05/06/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis si esibisce nuovamente al \"Milton Berle Show\" e in quell'occasione propone la canzone \"Hound dog\", in una versione a dir poco sensuale, definita poi dalla stampa scabrosa e volgare, che fa giungere alla redazione dello show milioni di lettere di critiche. Lo show di Milton Berle all'epoca era seguito come uno show per le famiglie e l'esibizione di Elvis aveva prodotto un vero scandalo...ma contribu� comunque ad accrescere, se possibile, ancora di pi� la sua popolarit�! Mentre le comunit� religiose e la moralit� borghese lo condannavano, i giovani lo amavano sempre di pi�!",
    "IMAGENAME":"19560605_elvis"
  },
  {
    "IDNEWS":18,
    "TITLE":"Elvis ha l'Ingaggio pi� alto mai pagato in un variet�",
    "DATE":"01/07/1956",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis si esibisce allo \"Steve Allen Show\" sulla NBC. Dato il senso dell'umorismo di Elvis, unito a quello del conduttore, si decide di giocare sulla sua immagine e per riscattarlo dallo scandalo dell'apparizione al \"Milton Berle Show\", gli viene fatto indossare un frac con le code e cantare \"Hound dog\" alla presenza di un cane basset hound... Man mano che crescono le registrazioni e continuano i suoi tour, in ugual misura crescono e si dilagano anche le controversie sul suo personaggio, facendo aumentare nel bene e nel male la sua popolarit�....\"Che se ne parli bene o che se ne parli male, l'importante � che se ne parli\", recita un vecchio adagio... Ed Sullivan, che fino a poco tempo prima disse che non si sarebbe mai avvalso della presenza di Elvis nel suo show, cambia presto idea quando viene a conoscenza delle grandi valutazioni che vengono fatte sul cantante dopo le sue apparizioni agli altri due shows. Un contratto di 50.000$ per tre apparizioni � ci� che ne esce. E' il pi� alto ingaggio mai pagato ad un cantante fino a quel momento per esibirsi in un variet�!",
    "IMAGENAME":"19560701_elvis"
  },
  {
    "IDNEWS":19,
    "TITLE":"Love me tender al cinema",
    "DATE":"16/11/1956",
    "TIME":"10:00",
    "DESCRIPTION":"al Paramount Theatre di New York il film \"Love Me Tender\" esce in prima visione, per poi uscire nelle sale cinematografiche nazionali il giorno seguente. La critica apprezza l'interpretazione di Elvis in questo melodramma ambientato nel sud America al tempo della Guerra Civile.",
    "IMAGENAME":"19561116_elvis"
  },
  {
    "IDNEWS":20,
    "TITLE":"The pelivis censurato",
    "DATE":"12/01/1957",
    "TIME":"10:00",
    "DESCRIPTION":"partecip� all'Ed Sullivan show, riscuotendo un successo clamoroso, ma la sua esibizione venne parzialmente censurata, poich� egli venne ripreso dalla cintola in su, onde evitare di inquadrare i suoi famosi e sconvenienti \"movimenti pelvici\"",
    "IMAGENAME":"19570112_elvis"
  },
  {
    "IDNEWS":21,
    "TITLE":"Ultimi 2 show prima della chiamata alle armi",
    "DATE":"15/03/1958",
    "TIME":"10:00",
    "DESCRIPTION":"si esibisce in 2 show a Memphis: queste due esibizioni sono le ultime prima della partenza per il servizio militare",
    "IMAGENAME":"19580315_elvis"
  },
  {
    "IDNEWS":22,
    "TITLE":"I Want YOU!",
    "DATE":"24/03/1958",
    "TIME":"10:00",
    "DESCRIPTION":"Il 24 marzo del 1958 viene arruolato e destinato in un centro d'addestramento in Texas con il numero di matricola US53310761| un servizio militare anomalo, sotto la costante presenza di giornalisti, fotografi e giovani fans che assediano ogni sua libera uscita| si congeda il 5 marzo 1960, torna sul palco e duetta con Frank Sinatra al \"Welcome Home Elvis\". ",
    "IMAGENAME":"19580324_elvis"
  },
  {
    "IDNEWS":23,
    "TITLE":"Gladys Presley, la madre di Elvis, muore.",
    "DATE":"14/08/1958",
    "TIME":"10:00",
    "DESCRIPTION":"Gladys Presley, la madre di Elvis, si ammala e ritorna a Memphis per essere ricoverata in ospedale a causa di un'epatite acuta. Elvis parte subito per Memphis e arriva nel pomeriggio del 12 agosto. Va a fare visita alla madre quella sera, il giorno dopo e la sera seguente, ma dopo qualche ora dal ritorno di Elvis a Graceland per riposare, Gladys muore: era il 14 agosto 1958 e Gladys aveva 46 anni. Elvis � completamente distrutto!!! La perdita della madre, a cui era molto legato e che per lui � sempre stata come una guida, � un evento che traccia un segno indelebile nella sua vita. ",
    "IMAGENAME":"19580814_elvis"
  },
  {
    "IDNEWS":24,
    "TITLE":"Intervista ABC per i 24 anni",
    "DATE":"08/01/1959",
    "TIME":"10:00",
    "DESCRIPTION":"in occasione del suo 24� compleanno, Elvis viene intervistato telefonicamente da Dick Clark per il suo show sulla ABC. \n Nel frattempo il Colonnello Parker continua a tenere viva la carriera di Elvis con promozioni e lancio di nuovi dischi in cui sono raccolti i suoi successi. \n",
    "IMAGENAME":"19590108_elvis"
  },
  {
    "IDNEWS":25,
    "TITLE":"Elvis e Priscilla si sposano",
    "DATE":"01/05/1967",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis e Priscilla si sposano con una cerimonia privata insieme ad un piccolo gruppo formato dalla famiglia e dagli amici all'Aladdin Hotel di Las Vegas. Segue una conferenza stampa. La luna di miele viene fatta per qualche giorno a Palm Springs per poi tornare a Graceland. E il 29 maggio Elvis e Priscilla indossano nuovamente i loro abiti nuziali e danno un secondo ricevimento di nozze a Graceland per la famiglia e gli amici che non erano presenti al matrimonio a Las Vegas. \nUn mese dopo Elvis � gi� al lavoro per il suo ventiseiesimo film, \"Speedway\", nel quale � affiancato da Nancy Sinatra. Durante le riprese viene annunciato che Priscilla � incinta. ",
    "IMAGENAME":"19670501_elvis"
  },
  {
    "IDNEWS":26,
    "TITLE":"Nasce la figlia del RE!",
    "DATE":"01/02/1968",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis diventa padre! il suo matrimonio viene allietato dalla nascita di una bambina, \"Lisa Marie\".\nPer tutto il corso della sua esistenza Elvis nutr� una vera e propria forma di adorazione per la figlia",
    "IMAGENAME":"19680201_elvis"
  },
  {
    "IDNEWS":27,
    "TITLE":"Elvis e Priscilla vanno in vacanza alle Hawaii",
    "DATE":"28/05/1968",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis e Priscilla vanno in vacanza alle Hawaii",
    "IMAGENAME":"19680528_elvis"
  },
  {
    "IDNEWS":28,
    "TITLE":"Elvis va in montagna",
    "DATE":"24/01/1969",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis, Priscilla e Lisa-Marie sono ad Aspen, nel Colorado, in compagnia dei soliti amici per una vacanza sugli sci.",
    "IMAGENAME":"19690124_elvis"
  },
  {
    "IDNEWS":29,
    "TITLE":"Parte lo Show!",
    "DATE":"03/08/1969",
    "TIME":"10:00",
    "DESCRIPTION":" Elvis viene ingaggiato per 4 settimane all'International Hotel di Las Vegas per un totale di 57 show: ha con s� musicisti rock, un orchestra, un gruppo gospel maschile e un gruppo soul/gospel femminile. \nL'orchestra � diretta da Bobby Morris, mentre i musicisti sono: James Burton (lead guitar), John Wilkinson (rhythm guitar), Jerry Sheff (bass guitar), Larry Muhoberac (piano) e Ronnie Tutt (batteria). Il gruppo femminile sono \"The Sweet Inspirations\" e il gruppo maschile sono \"The Imperials\". Charlie Hodge ha la funzione di seconda voce, chitarrista e assistente sul palco.Lo show comprende alcuni classici di successo di Elvis, ultimi lavori e alcune cover di altri artisti, il tutto naturalmente seguito da una conferenza stampa. E' un vero trionfo!!! Da questi show viene realizzato l'album \"Elvis in Person at the International Hotel\". \nI costumi indossati da Elvis sono sempre disegnati da Bill Belew, che negli anni seguenti disegner� tutti gli altri costumi di scena. ",
    "IMAGENAME":"19690803_elvis"
  },
  {
    "IDNEWS":30,
    "TITLE":"Fallimenti familiari per Elvis",
    "DATE":"01/01/1972",
    "TIME":"10:00",
    "DESCRIPTION":"Priscilla se ne va di casa , portando con s� la piccola Lisa Marie. Questa separazione dalla moglie e dalla figlia � un dolore che colpisce Elvis profondamente e che gli richieder� molto tempo per accettarlo, ma che comunque non gli dar� pace per il resto della sua vita. ",
    "IMAGENAME":"19720101_elvis"
  },
  {
    "IDNEWS":31,
    "TITLE":"Aloha from Hawaii via satellite",
    "DATE":"14/01/1973",
    "TIME":"10:00",
    "DESCRIPTION":"alle Isole Hawaii nella localit� di Honolulu, ripreso in mondovisione, pubblicato su vinile dalla RCA Records nel febbraio 1973 e che raggiunse la vetta della classifica Billboard 200 nella primavera seguente e la settima posizione in Norvegia. Sia lo special Tv che l'album della colonna sonora tratto dallo stesso, furono un enorme successo commerciale per Elvis, che sembr� rilanciare le sue quotazioni come artista, le quali erano in declino ormai gi� da un paio di anni. Nonostante le innovazioni tecnologiche del satellite (menzionato anche nel titolo del disco), negli Stati Uniti il concerto non fu trasmesso fino al 4 aprile 1973.",
    "IMAGENAME":"19730114_elvis"
  },
  {
    "IDNEWS":32,
    "TITLE":"Elvis torna a Menphis in concerto!",
    "DATE":"20/03/1973",
    "TIME":"10:00",
    "DESCRIPTION":"si esibisce a Memphis, cosa che non faceva dal 1961 e per soddisfare la richiesta di biglietti si vede costretto a dare 4 concerti in 2 giorni. Da una di queste 4 esibizioni ne esce l'album \"Elvis Recorded Live On Stage in Memphis\". E' inclusa una versione live di \"How Great Thou Art\", che lo porta a vincere il suo terzo Grammy Award. ",
    "IMAGENAME":"19730320_elvis"
  },
  {
    "IDNEWS":33,
    "TITLE":"Elvis si compra un jet personale per riprendere il suo tour dopo il lungo ricovero.",
    "DATE":"17/04/1975",
    "TIME":"10:00",
    "DESCRIPTION":"Il jet si chiama Lisa Marie, in onore alla figlia e riprende i concerti a Las Vegas, per completare la serie che era stato costretto a\nsospendere a causa del ricovero ospedaliero.",
    "IMAGENAME":"19750417_elvis"
  },
  {
    "IDNEWS":34,
    "TITLE":"Elvis registra nella \"Jungle Room\"",
    "DATE":"23/01/1976",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis � ritornato a Memphis per preparare una session speciale che far� a Graceland. Elvis ha voluto registrare a casa e perci� la RCA ha mandato un pullman speciale. Tutte le attrezzature sono poi state sistemate nella ?Jungle Room?.",
    "IMAGENAME":"19760123_elvis"
  },
  {
    "IDNEWS":35,
    "TITLE":"Elvis fa il suo ultimo concerto",
    "DATE":"26/06/1977",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis � ad Indianapolis per esibirsi all'Indiana's Market Square Arena: � il suo ultimo concerto, la sua\nultima salita sul palcoscenico!! Dopo questo concerto Elvis torna a Memphis per riposare e per preparare il suo nuovo tour di concerti, che dovrebbe\npartire dopo la met� di agosto....",
    "IMAGENAME":"19770626_elvis"
  },
  {
    "IDNEWS":36,
    "TITLE":"Muore Elvis e nasce la leggenda",
    "DATE":"16/08/1977",
    "TIME":"10:00",
    "DESCRIPTION":"Elvis Presley � stato trovato morto il 16 Agosto 1977 nel bagno della sua reggia di Graceland, con il viso nel suo stesso vomito.\nI medici che hanno fatto l'autopsia sul corpo da subito affermano che Elvis � morto per un infarto, causato, molto probabilmente, dal peso eccessivo del re del rock che nel 1977 pesava 160 Kg.\nMa sulla sua morte ci sono, in realt�, 4 ipotesi:\nla prima dice che Elvis sia deceduto per cause naturali, appunto l'infarto o una crisi respiratoria| \nla seconda pensa al suicidio per depressione, ma i pi� attenti dicono che Elvis si sarebbe suicidato in modo diverso e non in presenza della figlia| \nla terza pensa, invece, all'omicidio: un'ipotesi presa in considerazione anche dal padre del cantante che fece le sue indagini, assoldando anche un detective| \nla quarta � quella della messa in scena. \nSoffermiamoci soprattutto sulle ultime due, tenendo conto di due elementi importanti: sulla lapide di Elvis il suo secondo nome, Aron, � sbagliato ed � scritto con due A, cio� AARON, e il suo cadavere nella bara risulta essere troppo magro rispetto all'Elvis del 1977, tant'� che si suppone fosse una statua di cera.\nRitornando all'ipotesi di omicidio, si suppone che Elvis, cos� come Lennon, Janis Joplin, sia stato fatto fuori da chi non era d'accordo sulla diffusione di quest'icona di giovani svegli| altri, invece, credono che sia stato ucciso dai narcotrafficanti visto che il giorno dopo la sua morte Elvis avrebbe dovuto confessare contro dei narcos e visto che offriva la sua collaborazione a Nixon per combattere il traffico di droga.\nPer quanto riguarda l'ipotesi della messa in scena, bisogna aggiungere che Elvis aveva a che fare con il Priorato di Sion, una societ� segreta che combatte il comunismo internazionale e che lo avrebbe cos� \"nascosto\" agli occhi del mondo, fornendogli magari una nuova identit�.\nE in effetti c'� chi riconosce nel cantante Jon Cotner proprio il re del rock.\nJon Cotner si fa anche chiamare Mistery Man 1835, un nome piuttosto strano: addirittura, i fans riconoscono in quel numero la data di nascita di Elvis: 1 � il mese di nascita, 8 il giorno, 35 l'anno di nascita.\nLo stesso Jon Cotner era presente all'elezione di Obama nel 2009, ma da un'analisi della voce e del viso sono pochi i punti in comune.\nOltre a tutte queste ipotesi ce n'� un'ultima, forse pi� azzardata delle altre: quella che presume la provenienza di Elvis da un altro pianeta, un alieno, e che quindi non sia morto, ma ritornato al suo pianeta d'origine.",
    "IMAGENAME":"19770816_elvis"
  }
]